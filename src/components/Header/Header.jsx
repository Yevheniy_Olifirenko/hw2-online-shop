import React from 'react';
import '../../styles/Main.scss'
import ShopIcon from "../../../public/images/shop.png"
import StarIcon from "../../../public/images/star.png"
import StoreLogo from "../../../public/images/shoplogo.png"

const Header = ({ cartCount, favoriteCount }) => {
    return (
        <div className="header">
            <div className='shop-logo'>
                <img src={StoreLogo} alt="Shop Icon" className="shop-icon" />
            </div>

            <h1 className='header-text'>tea</h1>

            <div className='carts-wrapper'>
                <div className='carts'>
                    <img src={ShopIcon} alt="Cart" className="cart-icon" />
                    <span className="badge">{cartCount}</span>
                </div>

                <div className='carts'>
                    <img src={StarIcon} alt="Favorite" className="star-icon" />
                    <span className="badge">{favoriteCount}</span>
                </div>
            </div>
        </div>
    );
};


export default Header;