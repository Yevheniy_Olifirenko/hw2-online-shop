import React, { useState, useEffect } from 'react';
import ProductCard from '../ProductCard/ProductCard';
import Header from '../Header/Header';
import '../../styles/Main.scss';

const HomePage = () => {
    const [products, setProducts] = useState([]);
    const [cart, setCart] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [favoriteCount, setFavoriteCount] = useState(0);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('/products.json');
                const data = await response.json();
                setProducts(data);

                const storedCart = localStorage.getItem('cart');
                const storedFavorites = localStorage.getItem('favorites');

                if (storedCart) {
                    setCart(JSON.parse(storedCart));
                }

                if (storedFavorites) {
                    const parsedFavorites = JSON.parse(storedFavorites);
                    setFavorites(parsedFavorites);
                    setFavoriteCount(parsedFavorites.length);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    const addToCart = (product) => {
        setCart((prevCart) => {
            const updatedCart = [...prevCart, product];
            localStorage.setItem('cart', JSON.stringify(updatedCart));
            return updatedCart;
        });
    };

    const toggleFavorite = (product) => {
        setFavorites((prevFavorites) => {
            const updatedFavorites = prevFavorites.includes(product)
                ? prevFavorites.filter((fav) => fav !== product)
                : [...prevFavorites, product];
            localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
            setFavoriteCount(updatedFavorites.length);
            return updatedFavorites;
        });
    };

    return (
        <div className='wholeProducts'>
            <Header cartCount={cart.length} favoriteCount={favoriteCount} />
            <h2>Our Products</h2>
            <div className='productContainer'>
                {products.map((product) => (
                    <div key={product.article}>
                        <ProductCard
                            product={product}
                            addToCart={() => addToCart(product)}
                            toggleFavorite={() => toggleFavorite(product)}
                        />
                    </div>
                ))}
            </div>
        </div>
    );
};

export default HomePage;